@file:Suppress("UnstableApiUsage")

import kotlinx.kover.gradle.plugin.dsl.AggregationType
import kotlinx.kover.gradle.plugin.dsl.CoverageUnit
import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
    alias(libs.plugins.javaGradlePlugin)
    `embedded-kotlin`
    `kotlin-dsl`
    alias(libs.plugins.mavenPublish)
    alias(libs.plugins.gradlePluginPublish)
    alias(libs.plugins.kover)
    alias(libs.plugins.ktlint)
}

group = "com.pajato.plugins"
version = "0.9.5"

dependencies {
    implementation(libs.kotlinGradlePlugin)
    implementation(libs.kotlinSerialization)
    implementation(libs.koverGradlePlugin)
    implementation(libs.ktlintGradle)

    testImplementation(libs.kotlin.test)
    testImplementation(gradleTestKit())
}

testing {
    suites {
        val test by getting(JvmTestSuite::class) { useKotlinTest("2.0.20") }

        val functionalTest by registering(JvmTestSuite::class) {
            useKotlinTest("2.0.20")
            dependencies { implementation(project()) }
            targets { all { testTask.configure { shouldRunAfter(test) } } }
        }
    }
}

gradlePlugin {
    plugins.create("KmpLibPlugin") {
        id = "${project.group}.kmp-lib"
        implementationClass = "${project.group}.$name"
    }
}

repositories {
    gradlePluginPortal()
    mavenCentral()
    mavenLocal()
}

kotlin { explicitApi = ExplicitApiMode.Strict }

kover {
    reports.total.apply {
        html.onCheck.set(true)
        verify.rule {
            bound {
                aggregationForGroup = AggregationType.COVERED_PERCENTAGE
                coverageUnits = CoverageUnit.INSTRUCTION
                minValue = 85
            }
        }
    }
}

ktlint {
    additionalEditorconfig.set(
        mapOf(
            "ktlint_standard_function-signature" to "disabled",
            "ktlint_standard_try-catch-finally-spacing" to "disabled",
            "ktlint_standard_statement-wrapping" to "disabled",
            "ktlint_code_style" to "intellij_idea",
        ),
    )
    android.set(false)
    baseline.set(file("my-project-ktlint-baseline.xml"))
    coloredOutput.set(true)
    debug.set(true)
    enableExperimentalRules.set(false)
    ignoreFailures.set(false)
    outputColorName.set("RED")
    outputToConsole.set(true)
    version.set("1.1.1")
    verbose.set(true)
    kotlinScriptAdditionalPaths { include(fileTree("scripts/")) }
    filter {
        exclude("**/generated/**")
        include("**/kotlin/**")
    }
}
