plugins {
    alias(libs.plugins.kmpLib)
    `maven-publish`
}

group = "com.pajato.tks"
version = "0.9.0"
description = "A very simple project with no dependencies"

kotlin.sourceSets {
    val commonMain by getting { }

    val commonTest by getting { }
}
