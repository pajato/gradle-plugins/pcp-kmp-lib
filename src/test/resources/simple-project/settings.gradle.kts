@file:Suppress("UnstableApiUsage")

rootProject.name = "simple-project"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }

    plugins { id("com.pajato.plugins.kmp-lib") version "0.9.0" apply false }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}
