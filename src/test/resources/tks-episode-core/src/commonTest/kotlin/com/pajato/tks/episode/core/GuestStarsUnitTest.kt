package com.pajato.tks.episode.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class GuestStarsUnitTest : ReportingTestProfiler() {
    @Test fun `When a test guest stars object is serialized and deserialized, verify`() {
        val guestStars = GuestStars()
        val json = jsonFormat.encodeToString(guestStars)
        assertEquals("{}", json)
        assertEquals(-1, jsonFormat.decodeFromString<GuestStars>(json).id)
    }
}
