package com.pajato.tks.episode.core

import com.pajato.tks.common.core.ApiService
import com.pajato.tks.common.core.EpisodeKey

interface EpisodeRepo : ApiService {
    suspend fun getEpisode(key: EpisodeKey): Episode
}
