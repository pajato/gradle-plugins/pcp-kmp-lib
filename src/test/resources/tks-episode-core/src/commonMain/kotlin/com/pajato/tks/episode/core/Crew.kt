package com.pajato.tks.episode.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable data class Crew(
    val adult: Boolean = false,
    @SerialName("credit_id")
    val creditId: String = "",
    val gender: Int = 0,
    val department: String = "",
    @SerialName("episode_count")
    val episodeCount: Int = -1,
    @SerialName("known_for_department")
    val knownForDepartment: String = "",
    val id: Int = 0,
    val job: String = "",
    val name: String = "",
    @SerialName("original_name")
    val originalName: String = "",
    @SerialName("person_id")
    val personId: String = "",
    val popularity: Double = 0.0,
    @SerialName("profile_path")
    val profilePath: String = "",
)
