package com.pajato.plugins.kmp.lib

import kotlin.test.Test
import kotlin.test.assertEquals

class FixtureUnitTest {

    @Test fun `When adding 2 + 2, verify the result is 4`() {
        val result = 2 + 2
        val expected = 4
        assertEquals(expected, result)
    }
}
