package com.pajato.plugins.kmp.lib

import com.pajato.plugins.KmpLibPlugin
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import java.io.File
import kotlin.io.path.toPath
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class KmpLibPluginUnitTest {
    private val loader = this::class.java.classLoader

    @Test fun explore() {
        val dir: File = loader.getResource("tks-episode-core")?.toURI()?.toPath()?.toFile() ?: fail("No dir found")
        val project: Project = ProjectBuilder.builder().withProjectDir(dir).build()
        val plugin = KmpLibPlugin()
        plugin.apply(project)
        assertEquals(project.group, "")
    }
}
