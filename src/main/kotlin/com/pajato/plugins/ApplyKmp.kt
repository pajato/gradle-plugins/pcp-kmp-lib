package com.pajato.plugins

import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.withType
import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode.Strict
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinMultiplatformPluginWrapper

/**
 * Applies the Kotlin Multiplatform plugin and configuration to the specified project.
 *
 * @param project the Gradle project to which this function is applied
 */
internal fun applyKmp(project: Project) = with(project) {
    plugins.apply(KotlinMultiplatformPluginWrapper::class.java)
    extensions.getByType(KotlinMultiplatformExtension::class.java).apply { jvm(); explicitApi = Strict }
    tasks.withType<Test> { useJUnitPlatform() }
}
