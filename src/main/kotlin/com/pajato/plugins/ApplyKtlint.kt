package com.pajato.plugins

import org.gradle.api.Project
import org.jlleitschuh.gradle.ktlint.KtlintExtension

/**
 * Applies the Ktlint plugin and configures its extension for the specified project.
 *
 * @param project the Gradle project to which this function is applied
 */
internal fun applyKtlint(project: Project) = with(project) {
    plugins.apply("org.jlleitschuh.gradle.ktlint")
    extensions.getByType(KtlintExtension::class.java).apply { configureKtlintExtension(project) }
}

private fun KtlintExtension.configureKtlintExtension(project: Project) {
    additionalEditorconfig.set(getEditorConfigMap())
    android.set(false)
    baseline.set(project.file("my-project-ktlint-baseline.xml"))
    coloredOutput.set(true)
    debug.set(true)
    enableExperimentalRules.set(false)
    ignoreFailures.set(false)
    outputColorName.set("RED")
    outputToConsole.set(true)
    version.set("1.1.1")
    verbose.set(true)
    kotlinScriptAdditionalPaths { include(project.fileTree("scripts/")) }
    filter { exclude("**/generated/**"); include("**/kotlin/**") }
}

private fun getEditorConfigMap() = mapOf(
    "ktlint_standard_function-signature" to "disabled",
    "ktlint_standard_try-catch-finally-spacing" to "disabled",
    "ktlint_standard_statement-wrapping" to "disabled",
    "ktlint_code_style" to "intellij_idea",
)
