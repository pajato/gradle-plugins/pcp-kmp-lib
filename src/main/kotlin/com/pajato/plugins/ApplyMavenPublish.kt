package com.pajato.plugins

import org.gradle.api.Project

/**
 * Applies the Maven Publish plugin and configures its tasks for the specified project.
 *
 * @param project the Gradle project to which this function is applied
 */
internal fun applyMavenPublish(project: Project) = with(project) {
    plugins.apply("maven-publish")
    tasks.named("publish") { dependsOn("build") }
    tasks.named("publishToMavenLocal") { dependsOn("build") }
}
