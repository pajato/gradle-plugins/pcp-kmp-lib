package com.pajato.plugins

import kotlinx.kover.gradle.plugin.dsl.AggregationType.COVERED_PERCENTAGE
import kotlinx.kover.gradle.plugin.dsl.CoverageUnit.INSTRUCTION
import kotlinx.kover.gradle.plugin.dsl.KoverProjectExtension
import kotlinx.kover.gradle.plugin.dsl.KoverReportSetConfig
import kotlinx.kover.gradle.plugin.dsl.KoverVerifyRule
import org.gradle.api.Project
import org.gradle.kotlin.dsl.assign

/**
 * Applies the Kover plugin and configures coverage settings for the specified project.
 *
 * @param project the Gradle project to which this function is applied
 */
internal fun applyKover(project: Project) = with(project) {
    plugins.apply("org.jetbrains.kotlinx.kover")
    extensions.getByType(KoverProjectExtension::class.java).apply { configureKover() }
}

private fun KoverProjectExtension.configureKover() {
    reports.total.apply { configureReports(); configureFilters() }
}

private fun KoverReportSetConfig.configureFilters() {
    filters { excludes { annotatedBy("*Serializable"); annotatedBy("*Generated") } }
}

private fun KoverReportSetConfig.configureReports() {
    html.onCheck.set(true)
    verify.rule { configureBounds() }
}

private fun KoverVerifyRule.configureBounds() {
    bound { aggregationForGroup = COVERED_PERCENTAGE; coverageUnits = INSTRUCTION; minValue = 100 }
}
