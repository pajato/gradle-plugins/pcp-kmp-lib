package com.pajato.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * A Gradle plugin that applies various configurations to a Kotlin multiplatform library project.
 *
 * This plugin applies the following:
 * - Kotlin Multiplatform Plugin
 * - Ktlint Plugin for code linting
 * - Kover Plugin for code coverage
 * - Maven Publish Plugin
 * - Additional plugins specified by their IDs
 *
 * It also configures the `build` task to depend on `ktlintCheck`.
 */
public class KmpLibPlugin : Plugin<Project> {

    /**
     * Applies the necessary plugins, including serialization and Dokka, to the specified project.
     * Additionally, ensures that the ktlintCheck task is a dependency of the build task.
     *
     * @param project the Gradle project to which this function is applied
     */
    override fun apply(project: Project) {
        applyMainPlugins(project)
        applyOtherPlugins(project, listOf(SERIALIZATION_ID))
        project.tasks.named("build") { dependsOn("ktlintCheck") }
    }

    private fun applyMainPlugins(project: Project) {
        applyKmp(project)
        applyKtlint(project)
        applyKover(project)
        applyMavenPublish(project)
    }

    private fun applyOtherPlugins(project: Project, list: List<String>) { list.forEach { project.plugins.apply(it) } }

    public companion object {
        public const val SERIALIZATION_ID: String = "org.jetbrains.kotlin.plugin.serialization"
    }
}
