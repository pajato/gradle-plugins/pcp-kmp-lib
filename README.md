# pcp-kmp-lib

## Description

The Pajato library Kotlin multiplatform (KMP) convention plugin providing a common set of Gradle plugins to libary
projects. These include ktlint, ...

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

## Project status

WIP

## Documentation

## Usage

## Test Cases

### Overview

The table below identifies the unit tests. A unit test file name is always of the form
`NamePrefixUnitTest.kt`. The test file content is one or more test cases (functions)

| Filename Prefix  | Unit Test Case Name                                                                                |
|------------------|-----------------------------------------------------------------------------------------------|
| Fixture          | When adding two plus two, verify the result is four                                           |
| Id               | When accessing the plugin using a version, verify it is found                                 |
| PublishLocal     | When accessing the publishToLocal task, verify it is found                                    |
| PublishRemote    | When accessing the publish task, verify it is found                                           |
| Javadoc          | When accessing the dokkaJavadoc task, verify it is found                                      |
| Check            | When accessing the koverVerifhy task, verify it is found                                      |
| Coverage         | When accessing the koverVerify task, verify it is found                                       |
| Serialization    | When accessing the generateProjectStructureMetadata task, verify it is found                  |
| Kotlin           | Verify the Kotlin version is 2.0.0                                                            |
|------------------|-----------------------------------------------------------------------------------------------|


The table below identifies the unit tests. A functional test file name is always of the form
`NamePrefixFunctionalTest.kt`. The test file content is one or more test cases (functions)

| Filename Prefix  | Functional Test Case Name                                                                     |
|------------------|-----------------------------------------------------------------------------------------------|
| tbd              | When accessing the plugin using a version, verify it is found                                 |
|------------------|-----------------------------------------------------------------------------------------------|

### Notes

The single responsibility for this project is to provide an interface adapter layer that handles data conversions
between filter entity interfaces and persistable objects in order to execute filter use cases.

Examples of filter adapter artifacts are SerializableFilter and ArgusFilterRepo.
